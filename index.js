// console.log("Hello Wolrd!");

// [SECTION] Functions
	// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

// SYNTAX
// function keyword - used to defined a js function
// functionName  - functions are named to be able to use later in the code.
// function block ({}) - the statement which comprise the body of the function. This is where the code to be executed.
/*

function functionName(){
	//code block to be executed
}

*/

console.log("Hello!");

function printName(){
	console.log("My name is John!");
}

// Invocation or function calling
printName();
printName();
printName();

// Function Invocation
//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
//It is common to use the term "call a function" instead of "invoke a function".

// printName(); -> this is the function invocation.

// declaredFunction(); -> will result an error because we cannot call a function that is not defined/declared.

// Hoisting -> calling a function before its declaration
declaredFunction();

function declaredFunction(){
	console.log("Hello World form declaredFunction()");
}

declaredFunction();

// Function Expression
	//A function can also be stored in a variable. This is called a function expression.

	//A function expression is an anonymous function assigned to the variableFunction

	//Anonymous function - a function without a name.

let variableFunction = function(){
	console.log("Hello Again!");
}

variableFunction();

// We can also create a function expression of a named function.
// However, to invoke the function expression, we invoke it by its variable name, not by its function name.
// Function Expressions are always invoked (called) using the variable name.

let funcExpression = function funcName(){
	console.log("Hello from the other side.");
}

// funcName(); -> will result an error
funcExpression();

// Re-assining/updating a function expression

declaredFunction = function(){
	console.log("Updated declaredFunction");
}

declaredFunction();

funcExpression = function(){
	console.log("Updated funcExpression");
}

funcExpression();
funcExpression();
funcExpression();

const constantFunc = function(){
	console.log("Initialized with const!")
}

constantFunc();

/*constantFunc = function(){
	console.log("Cannot be updated!");
}

constantFunc();


Will result an error, cannot update a function stored in a const variable.
*/

// Function Scoping
/*	
	Scope is the accessibility (visibility) of variables within our program.
	
	Javascript Variables has 3 types of scope:
		1. local/block scope
		2. global scope
		3. function scope
*/

function showNames(){
	// Function Scope Variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();

// console.log(functionVar); -> will result an error cannot read variables inside the function
// console.log(functionConst);
// console.log(functionLet);

let globalVar = "Mr. WorldWide";

{
	let localVar = "Armando Perez";
	console.log(localVar); // this will work because the declaration and the usage of the variable is at the same scope.
	console.log(globalVar);
}

// console.log(localVar); will cause an error because the variable is enclose with curly braces or it is localy declared in curly braces.

// Nested Function
function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedName = "John";
		console.log(name);
	}
	// console.log(nestedName); -> will result an error, cannot access function scoped variable
	nestedFunction();
}

myNewFunction();
// nestedFunction(); -> will result an error

// Function and Glocal Scoped Variables

// Global Variable
let globalName = "Alexandro";

function myNewFunction2(){
	let nameInside = "Renz";

	console.log(globalName);
	// Global variable can be accessed inside a function
}

myNewFunction2();

// [SECTION] Return Statement
// To use reurtn statement we will be using "return" keyword

function returnFullName(){
	console.log("Hello");
	return "Jeffrey Smith Bezos";
	// console.log("Hello");
	// console.log("Hello");
	// console.log("Hello"); will never run becuase the return statement was declared before this line of codes.
}

let fullName = returnFullName();
console.log(fullName);

console.log(returnFullName());

function returnFullAddress(){
	let fullAddress = {
		street: "#44 Maharlika St.",
		City: "Cainta",
		province: "Rizal"

	}

	return fullAddress;
}

let myAddress = returnFullAddress();
console.log	(myAddress);

function printPlayerInfo(){
	console.log("Username: " + "white_knight");
	console.log("level: " + 95);
	console.log("Job: " + "Paladin");
}

let user1 = printPlayerInfo(); 
console.log(user1);

// You can return any data types from a function

function returnSumOf5and10(){
	return 5 + 10;
}
let SumOf5and10 = returnSumOf5and10();
console.log(SumOf5and10);

let total = 100 + returnSumOf5and10();
console.log(total);

// stimulates of getting an array of user names from a DB

function getGuildMembers(){
	return ["white_kngiht", "healer2000", "masterthief100"];

}

console.log(getGuildMembers());

// Function Naming Convention
// Funtion names should be defenitive of the task it will now perform.

function getCourses(){
	let courses = ["science", "math", "english"];
	return courses;
}

let courses = getCourses();
console.log(courses);

// avoid generic names













